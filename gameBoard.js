const gameBoard = length => {
  const misses = [];
  var ships = [];
  var gameBoard = new Array(length)
    .fill(0)
    .map(() => new Array(length).fill(0));

  const placeShips = (ship, x, y, orientation) => {
    for (var i = 0; i < ship.length; i++) {
      if (gameBoard[i + x][y] === 1 || gameBoard[x][i + y] === 1) {
        throw new Error("This coordinate is already taken");
      } else {
        if (orientation === "h") {
          gameBoard[i + x][y] = 1;
        }
        if (orientation === "v") {
          gameBoard[x][i + y] = 1;
        }
      }
      storeCoordinate(ship, x, y, orientation, ships);
    }
  };

  const storeCoordinate = (ship, xVal, yVal, orientation, array) => {
    array.push({ ship: ship, x: xVal, y: yVal, orientation: orientation });
  };

  const getStatus = (x, y) => {
    return gameBoard[x][y];
  };

  const getShipPosition = (x, y) => {
    for (var i = 0; i < ships.length; i++) {
      for (var j = 0; j < ships[i].ship.length; j++) {
        if (
          ships[i].orientation === "h" &&
          x === ships[i].x + j &&
          y === ships[i].y
        ) {
          return [i, j];
        }
        if (
          ships[i].orientation === "v" &&
          x === ships[i].x &&
          y === ships[i].y + j
        ) {
          return [i, j];
        }
      }
    }
    return false;
  };

  const recieveAttack = (x, y) => {
    const attack = getShipPosition(x, y);
    if (attack === false) {
      gameBoard[x][y] === 3;
      misses.push(x, y);
    } else {
      const shipNumber = attack[0];
      const position = attack[1];
      if (gameBoard[x][y] === 1) {
        gameBoard[x][y] === 2;
        ships[shipNumber].ship.hit(position);
      } else if (gameBoard[x][y] === 2) {
        throw new Error("This has already been hit");
      }
    }
  };

  const areAllShipsSunk = () => ships.every(obj => obj.ship.isSunk() === true);

  return {
    ships,
    gameBoard,
    placeShips,
    recieveAttack,
    getShipPosition,
    getStatus,
    areAllShipsSunk
  };
};

module.exports = gameBoard;
