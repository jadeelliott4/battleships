const ship = require("./battleships");
const gameBoard = require("./gameBoard");

test("Makes gameboard and fills with 0", () => {
  let testgameBoard = gameBoard(10);
  expect(testgameBoard.gameBoard[1][1]).toBe(0);
});

test("Places ship in correct position horizontally and sets status to 1", () => {
  let battleship2 = ship("ship1", 4);
  let testgameBoard2 = gameBoard(10);
  testgameBoard2.placeShips(battleship2, 2, 2, "h");
  expect(testgameBoard2.gameBoard[2][2]).toBe(1);
});

test("Throws error if tries to place ship in position which already has ship in it", () => {
  let testgameBoard4 = gameBoard(10);
  let battleship = ship("ship3", 2);
  let battleship2 = ship("ship3", 1);
  testgameBoard4.placeShips(battleship, 5, 5, "h");
  expect(() => {
    testgameBoard4.placeShips(battleship2, 5, 5, "h");
  }).toThrow();
});

test("Places ships in array", () => {
  let battleship4 = ship("ship2", 4);
  let testgameBoard3 = gameBoard(10);
  testgameBoard3.placeShips(battleship4, 5, 5, "h");
  expect(testgameBoard3.ships[0].x).toBe(5);
});

test("gets ships position in the array", () => {
  let battleship5 = ship("ship3", 4);
  let testgameBoard4 = gameBoard(10);
  testgameBoard4.placeShips(battleship5, 5, 5, "h");
  expect(testgameBoard4.getShipPosition(6, 5, "h")).toEqual([0, 1]);
});

test("recieves attack", () => {
  let battleship5 = ship("ship3", 4);
  let testgameBoard4 = gameBoard(10);
  testgameBoard4.placeShips(battleship5, 5, 5, "h");
  testgameBoard4.recieveAttack(5, 5);
  expect(battleship5.hits).toEqual([true, false, false, false]);
});

test("Throws error if tries to hit the same square twice", () => {
  let battleship2 = ship("ship3", 1);
  let testgameBoard4 = gameBoard(10);
  testgameBoard4.placeShips(battleship2, 2, 2, "h");
  testgameBoard4.recieveAttack(2, 2);
  expect(() => {
    testgameBoard4.recieveAttack(2, 2);
  }).toThrow();
});

test("all ships have been sunk", () => {
  let battleship = ship("ship3", 2);
  let battleship2 = ship("ship3", 1);
  let testgameBoard4 = gameBoard(10);
  testgameBoard4.placeShips(battleship, 5, 5, "h");
  testgameBoard4.placeShips(battleship2, 2, 2, "h");
  testgameBoard4.recieveAttack(5, 5);
  testgameBoard4.recieveAttack(6, 5);
  testgameBoard4.recieveAttack(2, 2);
  expect(testgameBoard4.areAllShipsSunk()).toBe(true);
});

test("all ships have not been sunk", () => {
  let battleship = ship("ship3", 2);
  let battleship2 = ship("ship3", 1);
  let testgameBoard4 = gameBoard(10);
  testgameBoard4.placeShips(battleship, 5, 5, "h");
  testgameBoard4.placeShips(battleship2, 2, 2, "h");
  testgameBoard4.recieveAttack(5, 5);
  testgameBoard4.recieveAttack(6, 5);
  expect(testgameBoard4.areAllShipsSunk()).toBe(false);
});
