const ship = require("./battleships");
const gameBoard = require("./gameBoard");

const player = () => {
  const attack = (secondGameboard, i, j) => {
    secondGameboard.recieveAttack(i, j);
  };
  const randomAttack = gameBoard => {
    let x = Math.floor(Math.random() * 11);
    let y = Math.floor(Math.random() * 11);
    gameBoard.recieveAttack(x, y);
  };
  return { attack, randomAttack };
};

module.exports = player;
