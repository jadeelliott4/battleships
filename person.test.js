const ship = require("./battleships");
const gameBoard = require("./gameBoard");
const player = require("./person");

test("player and computer should be able to make attacks", () => {
  let testgameBoard = gameBoard(10);
  let testgameBoard2 = gameBoard(10);
  let battleship = ship("ship1", 1);
  testgameBoard2.placeShips(battleship, 1, 1, "h");
  let player1 = player();
  let computer = player();
  player1.attack(testgameBoard2, 1, 1);
  computer.randomAttack(testgameBoard);
  expect(testgameBoard2.areAllShipsSunk()).toBe(true);
});
