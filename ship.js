const ship = (name, length) => {
  const hits = new Array(length).fill(false);
  const hit = position => {
    if (position >= length || position < 0) {
      throw new Error("You have not chosen a valid position");
    } else if (hits[position] === false) {
      hits[position] = true;
    } else {
      throw new Error("This square has already been hit");
    }
  };
  const isSunk = () => hits.every(square => square === true);

  return { name, length, hit, isSunk, hits };
};

module.exports = ship;
