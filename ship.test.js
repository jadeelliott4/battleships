const ship = require("./battleships");

const Ship = ship("ship1", 5);
test("Makes a ship with length", () => {
  expect(Ship.name).toBe("ship1");
  expect(Ship.length).toBe(5);
});

test("Ship isSunk false", () => {
  expect(Ship.isSunk()).toBe(false);
  Ship.hit(0);
  Ship.hit(2);
  expect(Ship.isSunk()).toBe(false);
});

test("Ship isSunk true", () => {
  Ship.hit(1);
  Ship.hit(3);
  Ship.hit(4);
  expect(Ship.isSunk()).toBe(true);
});

test("Throws error if not a valid position", () => {
  expect(() => {
    Ship.hit(10);
  }).toThrow();
});

test("Throws error if ship has already been hit", () => {
  expect(() => {
    Ship.hit(1);
  }).toThrow();
});
